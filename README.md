# FdF

`FdF` is a 3D Wireframe Viewer. **(macOS is only supported)**

![42.fdf map](/screenshots/42.png)

[`fdf.en.pdf`](/fdf.en.pdf) is the task file.
